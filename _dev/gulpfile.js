'use strict';

const gulp = require('gulp');
const cssimport = require("gulp-cssimport");
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify');

const destFolder = "../assets/";
const jsFiles = 'js/*.js';

let globalConfig = {
  src: 'css' // your dev stylesheet directory. No trailing slash
};

// Process CSS
gulp.task('styles', function(){
  return gulp.src(globalConfig.src + '/**/styles.scss.liquid')
    .pipe(cssimport())
    .pipe(gulp.dest(destFolder));
});

// Watch files
gulp.task('watch', function () {
  gulp.watch(globalConfig.src + '/**/*.scss.liquid', ['styles']);
  gulp.watch(jsFiles, ['js']);
});


//js
gulp.task('js', function () {
    return gulp.src(jsFiles)
        .pipe(gulp.dest(destFolder))
});

// Default task
gulp.task('default', ['watch', 'js']);
