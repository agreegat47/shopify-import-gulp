const common = (($) => {
    'use strict';

    /**
     * Some function
     *
     * @since   1.0.0
     */
    const someFunction = () => {
        // write some code here
        console.log('custom js work');
    };  

    /**
     * Fire events on document ready and bind other events
     *
     * @since   1.0.0
     */
    const ready = () => {
        //list of components
        someFunction();
    };

    /**
     * Only expose the ready function to the world
     */
    return {
        ready: ready
    }

})(jQuery);

jQuery(common.ready);
